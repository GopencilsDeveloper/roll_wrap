﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    public Rigidbody rigid;
    public float speed;

    #endregion

    #region PARAMS

    public bool canRotate;
    private Vector3 currentTouchPos;
    private Vector3 lastTouchPos;
    private Vector3 directionTouch;

    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    void Update()
    {
        if (canRotate/*  && !IsPointerOverUIObject() */)
        {
            rigid.centerOfMass = Vector3.zero;
            if (Application.isEditor)
            {
                Control();
            }
            else if (Application.isMobilePlatform)
            {
                if (Input.touchCount == 1)
                {
                    Control();
                }
            }
        }
    }

    private void Control()
    {
        if (Input.GetMouseButtonDown(0))
        {
            currentTouchPos = lastTouchPos = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            currentTouchPos = Input.mousePosition;
            directionTouch = currentTouchPos - lastTouchPos;
            directionTouch.x = Mathf.Clamp(directionTouch.x, -150f, 150f);
            directionTouch.y = Mathf.Clamp(directionTouch.y, -200f, 200f);

            lastTouchPos = currentTouchPos;
            rigid.AddTorque(new Vector3(directionTouch.y * 0.1f * speed, -directionTouch.x * 0.1f * speed, 0f), ForceMode.VelocityChange);
        }

        if (Input.GetMouseButtonUp(0))
        {
            directionTouch = Vector3.zero;
        }
    }

    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
#if UNITY_EDITOR
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
#elif UNITY_IOS || UNITY_ANDROID
        eventDataCurrentPosition.position = new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
#endif
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    #endregion

    #region DEBUG
    #endregion

}
