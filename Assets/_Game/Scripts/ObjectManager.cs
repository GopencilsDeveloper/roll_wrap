﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class ObjectManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    public List<GameObject> objectsList;
    public Transform objectParent;

    #endregion

    #region PARAMS

    public static ObjectManager Instance { get; private set; }
    public GameObject currentObject;
    public int index;

    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            NextMap();
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            BackMap();
        }
    }

    public void Refresh()
    {
        if (currentObject != null)
        {
            Destroy(currentObject.gameObject);
            currentObject = null;
        }
    }

    public void SpawnObject(int index = 0)
    {

        Refresh();

        currentObject = Instantiate(objectsList[index], Vector3.zero, Quaternion.identity, objectParent);

    }

    #endregion

    #region DEBUG

    [Button]
    public void NextMap()
    {
        index++;
        index = index > objectsList.Count - 1 ? 0 : index;

        SpawnObject(index);
    }

    [Button]
    public void BackMap()
    {
        index--;
        index = index < 0 ? objectsList.Count - 1 : index;

        SpawnObject(index);
    }


    #endregion

}
